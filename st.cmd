require essioc
require adsis8300bpm

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM11")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-130")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-4")
epicsEnvSet("EVR_NAME",       "PBI-BPM11:Ctrl-EVR-101:")
epicsEnvSet("SYSTEM1_PREFIX", "HEBT-030LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "HEBT 030 LWU BPM 01")
epicsEnvSet("SYSTEM2_PREFIX", "HEBT-040LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM2_NAME",   "HEBT 040 LWU BPM 01")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

